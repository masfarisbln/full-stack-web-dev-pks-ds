<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comments;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Comments::class, function (Faker $faker) {
    return [
        'content' => $faker->content,
        'post_id' => $faker->post_id
    ];
});
