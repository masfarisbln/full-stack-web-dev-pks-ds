<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Roles;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Roles::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
