<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*Route::get('/posts', 'PostController@index');
Route::post('/posts', 'PostController@store');*/

Route::apiResource('/posts', 'PostController');
Route::apiResource('/roles', 'RolesController');
Route::apiResource('/comments', 'CommentsController');

Route::group([
    'prefix' => 'auth' ,
    'namespace' => 'Auth'
] , function() {
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate-otp-code');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update-password');
    Route::post('login', 'LoginController')->name('auth.login');
    Route::post('posts', 'PostController@store')->name('posts');
    Route::post('comments', 'CommentsController@store')->name('comments');
});

Route::get('/test' , function(){
    return 'Masuk Pak Faris';
});//->middleware('auth:api');
?>