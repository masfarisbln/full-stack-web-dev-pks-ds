<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable = ['title', 'description', 'user_id'];
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot() {
        parent::boot();

        static::creating( function ($model) {
            $model->id = Str::uuid();

            $model->user_id = auth()->user()->id;
        });
    }

    public function comments() {
        return $this->hasMany('App\Comments');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
