<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class OtpCode extends Model
{
    protected $fillable = ['otp', 'user_id'];
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot() {
        parent::boot();

        static::creating( function ($model) {
            $model->id = Str::uuid();
        });
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
