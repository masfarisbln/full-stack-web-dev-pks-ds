<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $requestAll = $request->all();

        //set validation
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $users = User::where('email' , $request->email)->first();

        if($users->otp_code) {
            $users->otp_code->delete();
        }

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();

        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(60),
            'user_id' => $users->id
        ]);

        // Kirim email otp code ke email register

        return response()->json([
            'success' => true,
            'message' => 'OTP Code berhasil di generate',
            'data' => [
                'user' => $users,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
