<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $requestAll = $request->all();

        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $users = User::Create($requestAll);

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();

        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(60),
            'user_id' => $users->id
        ]);

        // Kirim email otp code ke email register

        return response()->json([
            'success' => true,
            'message' => 'Data user berhasil dibuat',
            'data' => [
                'user' => $users,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
