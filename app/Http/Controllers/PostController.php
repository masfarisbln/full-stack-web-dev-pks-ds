<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct() {
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar isi tabel Posts',
            'data'    => $posts  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->all();

        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        /*$posts = Posts::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);*/

        $posts = Posts::find($id);

        //success save to database
        if($posts) {

            $users = auth()->user();

            if($posts->user_id != $users->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Posts bukan milik user login',
                    'data'    => $posts  
                ], 403);
            }

            $posts->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data dengan judul ' . $posts->title . ' berhasil dibuat',
                'data'    => $posts  
            ], 200);

        } 


        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data tabel Posts gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $posts = Posts::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail isi tabel Posts',
            'data'    => $posts 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posts $posts)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $posts = Posts::findOrFail($posts->id);

        if($posts) {

            //update post
            $posts->update([
                'title'     => $request->title,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data tabel Posts berhasil diganti',
                'data'    => $posts  
            ], 200);
            
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Data tabel Posts tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find post by ID
        $posts = Posts::find($id);

        if($posts) {

            $users = auth()->user();

            if($posts->user_id != $users->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Posts bukan milik user login',
                    'data'    => $posts  
                ], 403);
            }

            //delete post
            $posts->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data tabel Posts berhasil dihapus',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Data tabel Posts tidak ditemukan',
        ], 404);
    }
}
