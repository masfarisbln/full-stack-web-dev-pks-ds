<?php

namespace App\Http\Controllers;

use App\Comments;
use Illuminate\Http\Request;
use App\Events\CommentStoreEvent;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comments::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar isi tabel Comments',
            'data'    => $comments  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->all();

        //set validation
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'post_id' => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comments = Comments::create([
            'content' => $request->content,
            'post_id' => $request->post_id
        ]);

        // memanggil event CommentStoreEvent
        event(new CommentStoreEvent($comments));

        //success save to database
        if($comments) {

            return response()->json([
                'success' => true,
                'message' => 'Data tabel Comments berhasil dibuat',
                'data'    => $comments  
            ], 200);

        } 


        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data tabel Comments gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find comments by ID
        $comments = Comments::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail isi tabel Comments',
            'data'    => $comments 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestAll = $request->all();

        

        //set validation
        $validator = Validator::make($requestAll, [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comments = Comments::find($id);

        /*$comments = Comments::create([
            'content' => $request->content,
            'post_id' => $request->post_id
        ]);*/

        if($comments) {

            $users = auth()->user();

            if($comments->user_id != $users->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Comments bukan milik user login',
                    'data'    => $comments  
                ], 403);
            }

            $comments->update([
                'content' => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data tabel Comments berhasil diganti',
                'data'    => $comments  
            ], 200);
            
        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'Data tabel Comments tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find comments by ID
        $comments = Comments::find($id);

        if($comments) {

            $users = auth()->user();

            if($comments->user_id != $users->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Posts bukan milik user login',
                    'data'    => $comments  
                ], 403);
            }

            //delete comments
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data tabel Comments berhasil dihapus',
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'Data dengan id ' . $id . ' tidak ditemukan',
        ], 404);
    }
}
