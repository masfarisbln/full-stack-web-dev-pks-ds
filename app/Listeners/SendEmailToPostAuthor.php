<?php

namespace App\Listeners;

use App\Events\CommentStoreEvent;
use App\Mail\PostAuthorMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToPostAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentStoreEvent  $event
     * @return void
     */
    public function handle(CommentStoreEvent $event)
    {
        Mail::to($event->comment->post->user->email)->send(new PostAuthorMail($event->comment));
    }
}
