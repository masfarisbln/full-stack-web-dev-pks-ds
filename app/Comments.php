<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = ['content', 'post_id', 'user_id'];
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot() {
        parent::boot();

        static::creating( function ($model) {
            $model->id = Str::uuid();

            $model->user_id = auth()->user()->id;
        });
    }

    public function post() {
        return $this->belongsTo('App\Posts');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
